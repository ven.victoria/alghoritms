﻿using System;
using System.Collections.Generic;

namespace Reverse_polish_notation
{
    class Program
    {
        static void Main(string[] args)
        {

            string str;
            Stack<double> stack = new Stack<double>();

            while ((str = Console.ReadLine()) != "exit")
            {
                double num;
                for (int i = 0; i < str.Length; i+=2)
                {
                    bool isNum = double.TryParse(str[i].ToString(), out num);
                    if (isNum)
                        stack.Push(num);
                    else
                    {
                        double op2;
                        switch (str[i])
                        {
                            case '+':
                                stack.Push(stack.Pop() + stack.Pop());
                                break;
                            case '*':
                                stack.Push(stack.Pop() * stack.Pop());
                                break;
                            case '-':
                                op2 = stack.Pop();
                                stack.Push(stack.Pop() - op2);
                                break;
                            case '/':
                                op2 = stack.Pop();
                                if (op2 != 0.0)
                                    stack.Push(stack.Pop() / op2);
                                else
                                    Console.WriteLine("Помилка! Ділення на нуль");
                                break;
                            case '^':
                                stack.Push(Math.Pow(stack.Pop(), stack.Pop()));
                                break;
                            case 's':
                                stack.Push(Math.Sqrt(stack.Pop()));
                                i += 3;
                                break;
                            case ' ':
                                Console.WriteLine("Результат: " + stack.Pop());
                                break;
                            default:
                                Console.WriteLine("Помилка! Хибна команда");
                                break;
                        }
                    }
                }
            }
        }
    }
}
