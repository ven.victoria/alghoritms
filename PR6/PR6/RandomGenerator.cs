﻿using System;

namespace PR1
{
    public static class GetRandomNumbers
    {
        private static Random _rnd = new Random();
        public static short[] GenerateShort(int count, short min, short max)
        {
            short[] arr = new short[count];

            for(int i = 0; i < count; i++)
            {
                arr[i] = Convert.ToInt16(_rnd.Next(min, max));
            }

            return arr;
        }
        public static double[] GenerateDouble(int count, double min, double max)
        {
            double[] arr = new double[count];

            for (int i = 0; i < count; i++)
            {
                arr[i] = _rnd.NextDouble() * (max - min) + min; ;
            }

            return arr;
        }

        public static float[] GenerateFloat(int count, double min, double max)
        {
            float[] arr = new float[count];

            for (int i = 0; i < count; i++)
            {
                arr[i] = (float)(_rnd.NextDouble() * (max - min) + min);
            }

            return arr;
        }


    }
}
