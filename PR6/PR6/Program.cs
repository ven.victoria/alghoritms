﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace PR1
{
    class Program
    {
        static void Main()
        {
            //HeapSort
            ArrayHeapSort(GetRandomNumbers.GenerateFloat(10, 10, 100));
            ArrayHeapSort(GetRandomNumbers.GenerateFloat(100, 10, 100));
            ArrayHeapSort(GetRandomNumbers.GenerateFloat(500, 10, 100));
            ArrayHeapSort(GetRandomNumbers.GenerateFloat(1000, 10, 100));
            ArrayHeapSort(GetRandomNumbers.GenerateFloat(2000, 10, 100));
            ArrayHeapSort(GetRandomNumbers.GenerateFloat(5000, 10, 100));
            ArrayHeapSort(GetRandomNumbers.GenerateFloat(10000, 10, 100));
            Console.ReadLine();
            //ShellSort
            shellSort(GetRandomNumbers.GenerateDouble(10, 0, 100));
            shellSort(GetRandomNumbers.GenerateDouble(100, 0, 100));
            shellSort(GetRandomNumbers.GenerateDouble(500, 0, 100));
            shellSort(GetRandomNumbers.GenerateDouble(1000, 0, 100));
            shellSort(GetRandomNumbers.GenerateDouble(2000, 0, 100));
            shellSort(GetRandomNumbers.GenerateDouble(5000, 0, 100));
            shellSort(GetRandomNumbers.GenerateDouble(10000, 0, 100));
            Console.ReadLine();
            //ArrayCountingSort
            ArrayCountingSort(GetRandomNumbers.GenerateShort(10, -100, 10));
            ArrayCountingSort(GetRandomNumbers.GenerateShort(100, -100, 10));
            ArrayCountingSort(GetRandomNumbers.GenerateShort(500, -100, 10));
            ArrayCountingSort(GetRandomNumbers.GenerateShort(1000, -100, 10));
            ArrayCountingSort(GetRandomNumbers.GenerateShort(2000, -100, 10));
            ArrayCountingSort(GetRandomNumbers.GenerateShort(5000, -100, 10));
            ArrayCountingSort(GetRandomNumbers.GenerateShort(10000, -100, 10));
            Console.ReadLine();
        }                                                            
        public static float[] ArrayHeapSort(float[] arr)
        {
            Stopwatch sw = new Stopwatch();
            Console.Write($"Heapsort {arr.Length}\n");
            int n = arr.Length;

            sw.Start();
            for (int i = n / 2 - 1; i >= 0; i--)
                heapify(arr, n, i);

            for (int i = n - 1; i >= 0; i--)
            {
                float temp = arr[0];
                arr[0] = arr[i];
                arr[i] = temp;

                heapify(arr, i, 0);
            }
            sw.Stop();
            Console.WriteLine($"Time {sw.ElapsedMilliseconds} ms");
            return arr;
        }

        public static double[] shellSort(double[] a)
        {
            Stopwatch sw = new Stopwatch();
            Console.Write($"Shellsort {a.Length}\n");
            double temp;
            int h = 0;
            sw.Start();
            while (h <= a.Length / 3)
                h = 2 * h + 1;

            for (int k = h; k > 0; k = (k - 1) / 3)
            {
                for (int i = k; i < a.Length; i++)
                {
                    temp = a[i];
                    int j;
                    for (j = i; j >= k; j -= k)
                    {
                        if (temp < a[j - k])
                            a[j] = a[j - k];
                        else
                            break;
                    }
                    a[j] = temp;
                }
            }
            sw.Stop();
            Console.WriteLine($"Time {sw.ElapsedMilliseconds} ms");
            return a;
        }

        public static short[] ArrayCountingSort(short[] arr)
        {
            short min = -100, max = 10;

            Dictionary<int, int> counts = new Dictionary<int, int>();
            Stopwatch sw = new Stopwatch();
            Console.Write($"Counting sort {arr.Length}\n");

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] < min)
                {
                    min = arr[i];
                }

                if (arr[i] > max)
                {
                    max = arr[i];
                }

                int count;

                counts.TryGetValue(arr[i], out count);
                counts[arr[i]] = count + 1;
            }

            int k = 0;

            for (short j = min; j <= max; j++)
            {
                int count;

                if (counts.TryGetValue(j, out count))
                {
                    for (int i = 0; i < count; i++)
                    {
                        arr[k++] = j;
                    }
                }
            }
            sw.Stop();
            Console.WriteLine($"Time {sw.ElapsedMilliseconds} ms");
            return arr;
        }

        static void heapify(float[] arr, int n, int i)
        {
            int largest = i;
            int l = 2 * i + 1;
            int r = 2 * i + 2;

            if (l < n && arr[l] > arr[largest])
                largest = l;

            if (r < n && arr[r] > arr[largest])
                largest = r;

            if (largest != i)
            {
                float swap = arr[i];
                arr[i] = arr[largest];
                arr[largest] = swap;

                heapify(arr, n, largest);
            }
        }
    }
}

