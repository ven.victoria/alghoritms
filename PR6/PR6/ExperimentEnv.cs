﻿//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Linq;

//namespace PR1
//{
//    public static class ExperimentEnv
//    {
//        static Stopwatch _sw = new Stopwatch();
//        static int[] _arraySizes = new int[]
//        {
//           10, 100, 500, 1000, 2000, 5000, 10000, 100000, 1000000, 10000000
//        };
//        public static event Action<int, long, string> ArraysSorted;

//        static ExperimentEnv()
//        {
//            ArraysSorted += Notification;
//        }

//        public static void ExperimentWithLinkedList(Func<LinkedList<int>, LinkedList<int>> sortFunction, Type t)
//        {
//            LinkedList<int> list;

//            foreach (var count in _arraySizes)
//            {
//                Console.Write($"Experiment with linked list ({count} items). Sort method - ");
//                list = new LinkedList<int>(RandomGenerator.Generate(count));

//                _sw.Start();
//                var sortedList = sortFunction(list);
//                _sw.Stop();
//                ArraysSorted(count, _sw.ElapsedMilliseconds, "Linked list");
//                _sw.Reset();
//            }

//            Console.WriteLine("\n");
//        }

//        public static void ExperimentWithArray(Func<int[], int[]> sortFunction)
//        {
//            int[] arr;

//            foreach (var count in _arraySizes)
//            {
//                Console.Write($"Experiment with array ({count} items). Sort method - ");
//                arr = RandomGenerator.Generate(count);
//                //DisplayArr(arr);
//                _sw.Start();
//                var sortedArray = sortFunction(arr);
//                //DisplayArr(arr);
//                _sw.Stop();

//                ArraysSorted(count, _sw.ElapsedMilliseconds, "Array");
//                _sw.Reset();
//            }

//            Console.WriteLine("\n");
//        }

//        private static void Notification(int count, long time, string item) 
//            => Console.WriteLine($"{item} with {count} items was sorted for {time} milliseconds\n");

//        private static void DisplayArr(IEnumerable<int> arr)
//        {
//            foreach(var item in arr)
//            {
//                Console.WriteLine(item);
//            }
//            Console.WriteLine("----------------------------------------------");
//        }
//    }
//}
