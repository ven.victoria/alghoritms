﻿#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <windows.h>
#include <cstdlib>
#include <iostream>

using namespace std;

int N = 0;
struct time
{
	unsigned short sec : 7;
	unsigned short min : 7;
	unsigned short hour : 6;
	unsigned short week_day : 4;
	unsigned short day_num : 6;
	unsigned short mounth : 6;
	unsigned short year : 8;
};

time* localTime = (time*)malloc((N + 1) * sizeof(time));

void DayNumber(int day)
{
	switch (day)
	{
	case 1: printf(" Понеділок, ");
		break;
	case 2: printf("Вівторок, ");
		break;
	case 3: printf("Середа, ");
		break;
	case 4: printf("Четвер, ");
		break;
	case 5: printf("П'ятниця, ");
		break;
	case 6: printf("Субота, ");
		break;
	case 7: printf("Неділя, ");
		break;
	default:
		printf("Такого дня не існує");
		break;
	}
}

void Get() {
	localTime[0].sec = 32;
	localTime[0].min = 42;
	localTime[0].hour = 11;
	localTime[0].week_day = 4;
	localTime[0].day_num = 6;
	localTime[0].mounth = 10;
	localTime[0].year = 20;

	printf("Date: ");
	DayNumber(localTime[0].week_day);
	printf("%d.%d.%d - %d:%d:%d", localTime[0].day_num,
		localTime[0].mounth, localTime[0].year, localTime[0].hour, localTime[0].min,
		localTime[0].sec);
}

struct struct2 {
	signed short n;
}my_struct[24];


union global {
	union num {
		struct n {
			unsigned short a0 : 1;
			unsigned short a1 : 1;
			unsigned short a2 : 1;
			unsigned short a3 : 1;
			unsigned short a4 : 1;
			unsigned short a5 : 1;
			unsigned short a6 : 1;
			unsigned short a7 : 1;
			unsigned short a8 : 1;
			unsigned short a9 : 1;
			unsigned short a10 : 1;
			unsigned short a11 : 1;
			unsigned short a12 : 1;
			unsigned short a13 : 1;
			unsigned short a14 : 1;
			unsigned short a15 : 1;
		}numer;
		signed short count;
	}num1;

	union numbers {
		struct number {
			unsigned short b0 : 1;
			unsigned short b1 : 1;
			unsigned short b2 : 1;
			unsigned short b3 : 1;
			unsigned short b4 : 1;
			unsigned short b5 : 1;
			unsigned short b6 : 1;
			unsigned short b7 : 1;
			unsigned short b8 : 1;
			unsigned short b9 : 1;
			unsigned short b10 : 1;
			unsigned short b11 : 1;
			unsigned short b12 : 1;
			unsigned short b13 : 1;
			unsigned short b14 : 1;
			unsigned short b15 : 1;
			unsigned short b16 : 1;
			unsigned short b17 : 1;
			unsigned short b18 : 1;
			unsigned short b19 : 1;
			unsigned short b20 : 1;
			unsigned short b21 : 1;
			unsigned short b22 : 1;
			unsigned short b23 : 1;
			unsigned short b24 : 1;
			unsigned short b25 : 1;
			unsigned short b26 : 1;
			unsigned short b27 : 1;
			unsigned short b28 : 1;
			unsigned short b29 : 1;
			unsigned short b30 : 1;
			unsigned short b31 : 1;
		}number;

		struct num3 {
			unsigned short val1 : 8;
			unsigned short val2 : 8;
			unsigned short val3 : 8;
			unsigned short val4 : 8;
		}num4;
		float val;
	}num2;
}global;


int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	printf("Об’єм пам’яті, що займає структура: %d", sizeof(time));
	int n = 1;
	while (n != 0)
	{
		printf("\nОберіть пункт меню\n 1. Вивести час\n 0. Вихід\n");
		scanf_s("%d", &n);
		switch (n) {
		case 1:
			Get();
			break;
		case 0:
			exit(0);
		}
	}

	int N = 0;
	int m;
	printf("Введіть кількість елементів для опрацювання: \n");
	scanf_s("%d", &N);
	printf("Введіть елементи:\n");
	for (m = 0; m < N; m++)
		scanf_s("%d", &my_struct[m].n);
	printf("[РЕЗУЛЬТАТ]\n");
	for (int a = 0; a < N; a++)
	{
		if (my_struct[a].n > 0) {
			printf("додатнє, значення: ");
			printf("%d\n", my_struct[a].n);
		}
		else if (my_struct[a].n < 0)
		{
			printf("від'ємне, значення: ");
			printf("%d\n", abs(my_struct[a].n));
		}
		else {
			printf("нейтральне, значення: ");
			printf("%d\n", my_struct[a].n);
		}
	}


	signed char a = 5, b = 127, с, k;
	с = a + b;
	cout << "5 + 127 = " << int(с) << endl;
	a = 2;
	b = 3;
	k = a - b;
	cout << "2 - 3 = " << int(k) << endl;
	a = -120;
	b = -34;
	k = a + b;
	cout << "- 120 - 34 = " << int(k) << endl;
	a = unsigned char(-5);
	cout << "(unsigned char) (-5) = " << int(a) << endl;
	a = 56;
	b = 38;
	k = a & b;
	cout << "56 & 38 = " << int(k) << endl;
	k = a | b;
	cout << "56 | 38 = " << int(k) << endl;


	system("chcp 1251");
	system("cls");
	float a;
	printf("Введіть елемент: ");
	scanf_s("%f", &global.num2.val);
	printf("%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
		global.num2.number.b31, global.num2.number.b30, global.num2.number.b29, global.num2.number.b28, global.num2.number.b27, global.num2.number.b26,
		global.num2.number.b25, global.num2.number.b24, global.num2.number.b23, global.num2.number.b22, global.num2.number.b21, global.num2.number.b20,
		global.num2.number.b19, global.num2.number.b18, global.num2.number.b17, global.num2.number.b16, global.num2.number.b15, global.num2.number.b14,
		global.num2.number.b13, global.num2.number.b12, global.num2.number.b11, global.num2.number.b10, global.num2.number.b9, global.num2.number.b8,
		global.num2.number.b7, global.num2.number.b6, global.num2.number.b5, global.num2.number.b4, global.num2.number.b3, global.num2.number.b2,
		global.num2.number.b1, global.num2.number.b0);
	printf("\n");
	printf("%d %d %d %d",


		global.num2.num4.val1, global.num2.num4.val2, global.num2.num4.val3,
		global.num2.num4.val4);
	printf("\n");
	printf("Мантиса: %d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d%d\n", global.num2.number.b22, global.num2.number.b21, global.num2.number.b20,
		global.num2.number.b19, global.num2.number.b18, global.num2.number.b17, global.num2.number.b16, global.num2.number.b15, global.num2.number.b14,
		global.num2.number.b13, global.num2.number.b12, global.num2.number.b11, global.num2.number.b10, global.num2.number.b9, global.num2.number.b8,
		global.num2.number.b7, global.num2.number.b6, global.num2.number.b5, global.num2.number.b4, global.num2.number.b3, global.num2.number.b2,
		global.num2.number.b1, global.num2.number.b0);
	printf("Знак: %d\n", global.num2.number.b31);
	printf("Степінь: %d%d%d%d%d%d%d%d\n", global.num2.number.b30, global.num2.number.b29,
		global.num2.number.b28, global.num2.number.b27, global.num2.number.b26, global.num2.number.b25, global.num2.number.b24,
		global.num2.number.b23);
	printf("Структура займає %d байт", sizeof(global));


	return 0;
}
