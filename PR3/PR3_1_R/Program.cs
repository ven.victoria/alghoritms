﻿using System;

namespace PR3_1_R
{
    class Program
    {
        private static int Factorial(int n) => n == 0 ? 1 : n * Factorial(n - 1);

        static void Main(string[] args)
        {
            Func<int, double>[] tabFuncs =
            {
                n => n,
                n => n * n,
                n => Math.Log(n),
                n => n * Math.Log(n),
                n => Math.Pow(2, n),
                n => Factorial(n)
            };

            foreach (var func in tabFuncs)
            {
                Environment.Tabulate(func);
            }
        }
    }
}
