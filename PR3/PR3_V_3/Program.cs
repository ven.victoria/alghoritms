﻿using System;
using System.Diagnostics;

namespace PR3_V_3
{
    public static class Program
    {
        private static int[] GenArr(int length)
        {
            Random rnd = new Random();
            int[] arr = new int[length];

            for (int i = 0; i < length; i++)
            {
                arr[i] = rnd.Next(0, 10);
            }
            return arr;
        }
        private static int GetMax(this int[] arr)
        {
            int max = 0;
            for(int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > max)
                    max = arr[i];
            }
            return max;
        }
        private static void PrintArr(this int[] arr)
        {
            for(int i = 0; i < arr.Length; i++)
            {
                Console.Write($"{arr[i]} ");
            }
        }
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Enter length of arr->");
                int.TryParse(Console.ReadLine(), out int length);

                var arr = GenArr(length);
                Stopwatch sw = new Stopwatch();
                var max = arr.GetMax();
                sw.Stop();
                arr.PrintArr();
                Console.WriteLine("\n Max value: " + max);
                Console.WriteLine(sw.Elapsed);
            }
        }
    }
}
