﻿using System;
using System.Diagnostics;

namespace PR3_Vika
{
    public static class Program
    {
        public static long Pow(this long a, long b)
        {
            var newNum = a;
            for (int i = 0; i < b - 1; i++)
            {
                newNum *= a;
            }
            return newNum;
        }
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Enter a->");
                long.TryParse(Console.ReadLine(), out long a);
                Console.Write("Enter b->");
                long.TryParse(Console.ReadLine(), out long b);
                Stopwatch sw = new Stopwatch();
                sw.Start();
                Console.WriteLine($"{a}^{b} = {a.Pow(b)}");
                sw.Stop();
                Console.WriteLine(sw.ElapsedMilliseconds);
            }
        }
    }
}
